//filter samsung or iphone
const renderPhoneSelected = () => {
  let condition = document.getElementById("phoneType").value;
  if (condition == "-1") showListOfProducts(productList);
  else {
    let result = productList.filter((item) => {
      return item.type == condition;
    });
    showListOfProducts(result);
  }
};

//handle add to cart
const addItemToCart = (id) => {
  console.log(productList);
  let index = productList.findIndex((item) => {
    return item.id == id;
  });
  let indexCart = cart.findIndex((item) => {
    return item.product.id == id;
  });
  if (indexCart == -1) {
    let cartItem = { product: { ...productList[index] }, quantity: 1 };
    cart.push(cartItem);
  } else cart[indexCart].quantity++;
  renderCartList(cart);
  luuLocalStorage();
};

//handle + - button
const handleTangGiamSoLuong = (id, giaTri) => {
  let index = cart.findIndex((item) => {
    return item.product.id == id;
  });
  cart[index].quantity += giaTri;
  if (cart[index].quantity == 0) cart.splice(index, 1);
  renderCartList(cart);
  luuLocalStorage();
};

//handle delete item
const deleteSp = (id) => {
  let index = cart.findIndex((item) => {
    return item.product.id == id;
  });
  cart.splice(index, 1);
  renderCartList(cart);
  luuLocalStorage();
};

//handle purchase
const handlePurchase = () => {
  if (confirm("Xác nhận thanh toán?")) {
    cart = [];
    renderCartList(cart);
    luuLocalStorage();
    alert("Thanh toán thành công!!!");
  }
};
