let productList = [];
let cart = [];

const BASE_URL = "https://6271e18725fed8fcb5ec0d4f.mockapi.io/dien-thoai";
const CART_LOCALSTORAGE = "CART_LOCALSTORAGE";

const renderCartList = (cart) => {
  let totalItem = 0;
  let contentHTML = "";
  let totalPurchase = 0;
  if (cart.length == 0) {
    contentHTML = `<h3 class="text-center text-secondary"> Giỏ hàng trống </h3>`;
    document.getElementById("totalCart").style.paddingLeft = "550px";
    document.getElementById("totalCart").innerHTML = "";
    document.getElementById("btnPurchase").style.display = "none";
  } else {
    cart.forEach((item) => {
      let contentTrTag = `
          <tr>
            <td>
              <img style="width:80px" src=${item.product.img} />
            </td>
            <td>${item.product.name}</td>
            <td>
              <button
                class="btn btn-success"
                onclick="handleTangGiamSoLuong('${item.product.id}',1)"
                style="width:40px;height:40px"
              >
                +
              </button>
              <span class="mx-3">${item.quantity} </span>
              <button
                class="btn btn-secondary"
                onclick="handleTangGiamSoLuong('${item.product.id}',-1)"
                style="width:40px;height:40px"
              >
                -
              </button>
            </td>
            <td>${item.product.price * item.quantity}</td>
            <td>
            <button onclick="deleteSp('${
              item.product.id
            }')" class="btn btn-danger">Xóa</button>
            </td>
          </tr>
        `;
      contentHTML += contentTrTag;
      totalPurchase += item.product.price * item.quantity;
    });
    document.getElementById("totalCart").style.paddingLeft = "550px";
    document.getElementById("totalCart").innerHTML = `
    Tổng tiền: <span class="text-danger">${totalPurchase}</span>
    `;
    document.getElementById("btnPurchase").style.display = "block";
  }
  cart.forEach((item) => {
    totalItem += item.quantity;
  });
  document.getElementById("cartList").innerHTML = contentHTML;
  document.getElementById("soLuong").innerHTML = totalItem;
};
renderCartList(cart);

//save data to local storage
const luuLocalStorage = function () {
  var cartJson = JSON.stringify(cart);
  localStorage.setItem(CART_LOCALSTORAGE, cartJson);
};
//get data to show when load web
cartJson = localStorage.getItem(CART_LOCALSTORAGE);
if (cartJson) {
  cart = JSON.parse(cartJson);
  renderCartList(cart);
}

//get list of products from API and show
const renderListOfProducts = () => {
  axios({
    url: BASE_URL,
    method: "GET",
  })
    .then((res) => {
      res.data.forEach((item) => {
        productList.push(item);
      });
      showListOfProducts(productList);
    })
    .catch((err) => {
      console.log(err);
    });
};

//show list of data when web start
renderListOfProducts();

//show list of product with data from API
const showListOfProducts = (list) => {
  let contentHTMl = "";
  list.forEach((item) => {
    let contentItem = /*html*/ `
      <div class="col-3 p-1">
        <div class="card position-relative " style="height:490px">
          <img src=${item.img} class="card-img-top pt-2" alt="..." />
          <div class="card-body">
            <h5 class="card-title">${item.name}</h5>
            <p class="card-text">
            <p class=" bg-info d-inline px-3 py-1 rounded">${item.price}</p>
            <p class="mt-2">${item.desc}</p>
            </p>
            <div class="buttonItem">
              <a data-toggle="modal" data-target="#itemModal" onclick="showDetailOfProduct('${item.id}')" class="btn btn-warning" style="cursor:pointer"
                >Detail</a>
              <a onclick="addItemToCart('${item.id}')" class="btn btn-success" style="cursor:pointer">Add to cart</a>
            </div>
          </div>
        </div>
      </div>
    `;
    contentHTMl += contentItem;
  });
  document.getElementById("productList").innerHTML = contentHTMl;
};

//show detail of products
const showDetailOfProduct = (item) => {
  let index = productList.findIndex((sp) => {
    return sp.id == item;
  });
  let sp = productList[index];
  document.getElementById("modalDetail").innerHTML = /*html*/ `
    <!-- Modal -->
    <div
      class="modal fade"
      id="itemModal"
      tabindex="-1"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">${sp.name}</h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body row">
            <div class="col-5">
              <img src=${sp.img} class="w-100" alt="" />
            </div>
            <div class="col-7">
              <h5>Thông số kĩ thuật</h5>
              <table class="table">
                <tbody>
                  <tr>
                    <td>Screen</td>
                    <td>${sp.screen}</td>
                  </tr>
                  <tr>
                    <td>Camera trước</td>
                    <td>${sp.frontCamera}</td>
                  </tr>
                  <tr>
                    <td>Camera sau</td>
                    <td>${sp.backCamera}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button
              type="button"
              class="btn btn-secondary"
              data-dismiss="modal"
            >
              Close
            </button>
          </div>
        </div>
      </div>
    </div>
  `;
};
