let validator = new Validation();

const handleDeleteProduct = (id) => {
  turnOnLoading();
  productService
    .deleteData(id)
    .then((res) => {
      turnOffLoading();
      renderListOfProduct();
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
};

const handleAddProduct = () => {
  let newSanPham = layThongTinTuForm();
  productService
    .getList()
    .then((res) => {
      let isValidTen =
        validator.kiemTraRong("TenSP", "tbTenSp", "Tên không hợp lệ") &&
        validator.kiemTraTenHopLe(res.data);
      let isValidGia =
        validator.kiemTraRong("GiaSP", "tbGiaSp", "Giá không hợp lệ") &&
        validator.kiemTraGiaHopLe();
      let isValidHinh = validator.kiemTraRong(
        "HinhSP",
        "tbHinhSp",
        "Hình không hợp lệ"
      );
      let isValidMoTa =
        validator.kiemTraRong("MoTa", "tbMoTaSp", "Mô tả không hợp lệ") &&
        validator.kiemTraMoTaHopLe();
      let isValid = isValidTen && isValidHinh && isValidMoTa && isValidGia;
      if (isValid) {
        productService
          .addNewData(newSanPham)
          .then((res) => {
            $("#myModal").modal("hide");
            renderListOfProduct();
          })
          .catch((err) => {
            console.log(err);
          });
      }
    })
    .catch((err) => {});
};

const handleEditProduct = () => {
  let editSp = layThongTinTuForm();
  let id = document.querySelector("#IDsp span").innerText;
  productService
    .getList()
    .then((res) => {
      let isValidName;
      res.data.forEach((item) => {
        if (item.name == editSp.name && item.id != id) {
          document.getElementById("tbTenSp").innerText = "Sản phẩm đã tồn tại";
          isValidName = false;
        } else {
          document.getElementById("tbTenSp").innerText = "";
          isValidName = true;
        }
      });
      isValidName =
        isValidName &&
        validator.kiemTraRong("TenSP", "tbTenSp", "Tên không hợp lệ");
      let isValidGia =
        validator.kiemTraRong("GiaSP", "tbGiaSp", "Giá không hợp lệ") &&
        validator.kiemTraGiaHopLe();
      let isValidHinh = validator.kiemTraRong(
        "HinhSP",
        "tbHinhSp",
        "Hình không hợp lệ"
      );
      let isValidMoTa =
        validator.kiemTraRong("MoTa", "tbMoTaSp", "Mô tả không hợp lệ") &&
        validator.kiemTraMoTaHopLe();
      let isValid = isValidName && isValidHinh && isValidMoTa && isValidGia;
      if (isValid) {
        productService
          .changeData(editSp, id)
          .then((res) => {
            renderListOfProduct();
            $("#myModal").modal("hide");
          })
          .catch((err) => {});
      }
    })
    .catch((err) => {});
};

const handleTimKiemThongTin = () => {
  let searchValue = document.getElementById("inputTK").value.trim();
  turnOnLoading();
  productService
    .getList()
    .then((res) => {
      turnOffLoading();
      let result = res.data.filter((item) => {
        return item.name.trim() == searchValue;
      });
      showListOfProducts(result);
    })
    .catch((err) => {
      turnOffLoading();
    });
};

document.getElementById("btnThemSP").addEventListener("click", () => {
  document.getElementById("tbTenSp").innerText = "";
  document.getElementById("tbGiaSp").innerText = "";
  document.getElementById("tbHinhSp").innerText = "";
  document.getElementById("tbMoTaSp").innerText = "";
  document.getElementById("form-item").reset();
  document.querySelector("#IDsp").style.display = "none";
  document.getElementById("btnEdit").style.display = "none";
  document.getElementById("btnAdd").style.display = "block";
});
