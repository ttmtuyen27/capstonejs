const turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

const turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};

document.querySelector("#IDsp").style.display = "none";

const renderListOfProduct = () => {
  turnOnLoading();
  productService
    .getList()
    .then((res) => {
      turnOffLoading();
      showListOfProducts(res.data);
    })
    .catch((err) => {
      turnOffLoading();
      console.log(err);
    });
};

renderListOfProduct();

const showListOfProducts = (list) => {
  let contentHTML = "";
  list.forEach((item) => {
    let contentTrTag = `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.price}</td>
        <td>${item.img}</td>
        <td>${item.desc}</td>
        <td>
        <button class="btn btn-danger" onclick="handleDeleteProduct(${item.id})">Xóa</button>
        <button class="btn btn-info" onclick="xuatThongTinLenForm(${item.id})">Sửa</button>
        </td>
        </tr>
        `;
    contentHTML += contentTrTag;
  });
  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

const layThongTinTuForm = () => {
  let name = document.getElementById("TenSP").value;
  let price = document.getElementById("GiaSP").value;
  let img = document.getElementById("HinhSP").value;
  let desc = document.getElementById("MoTa").value;
  return new SanPham(name, price, img, desc);
};

const xuatThongTinLenForm = (id) => {
  turnOnLoading();
  document.getElementById("btnAdd").style.display = "none";
  document.getElementById("btnEdit").style.display = "block";
  productService
    .getData(id)
    .then((res) => {
      turnOffLoading();
      $("#myModal").modal("show");
      document.querySelector("#IDsp span").innerText = res.data.id;
      document.querySelector("#IDsp").style.display = "block";
      document.getElementById("TenSP").value = res.data.name;
      document.getElementById("GiaSP").value = res.data.price;
      document.getElementById("HinhSP").value = res.data.img;
      document.getElementById("MoTa").value = res.data.desc;
      document.getElementById("tbTenSp").innerText = "";
      document.getElementById("tbGiaSp").innerText = "";
      document.getElementById("tbHinhSp").innerText = "";
      document.getElementById("tbMoTaSp").innerText = "";
    })
    .catch((err) => {
      turnOffLoading();
    });
};
