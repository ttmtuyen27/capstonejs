const BASE_URL = "https://629ac2b0cf163ceb8d0e809d.mockapi.io/san-pham";

const productService = {
  getList: function () {
    return axios({
      url: `${BASE_URL}`,
      method: "GET",
    });
  },
  changeData: function (editSp, id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "PUT",
      data: editSp,
    });
  },
  deleteData: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "DELETE",
    });
  },
  getData: function (id) {
    return axios({
      url: `${BASE_URL}/${id}`,
      method: "GET",
    });
  },
  addNewData: function (newSanPham) {
    return axios({
      url: `${BASE_URL}`,
      method: "POST",
      data: newSanPham,
    });
  },
};
