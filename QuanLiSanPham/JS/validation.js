function Validation() {
  //input không được rỗng
  this.kiemTraRong = (idTarget, idError, message) => {
    let targetValue = document.getElementById(idTarget).value;
    if (!targetValue) {
      document.getElementById(idError).innerText = message;
      return false;
    } else {
      document.getElementById(idError).innerText = "";
      return true;
    }
  };

  //giá là số thập phân hoặc số nguyên
  this.kiemTraGiaHopLe = () => {
    let price = document.getElementById("GiaSP").value;
    let pattern = /^\d{0,8}(\.\d{1,4})?$/;
    if (!pattern.test(price)) {
      document.getElementById("tbGiaSp").innerText = "Giá không hợp lệ";
      return false;
    } else {
      document.getElementById("tbGiaSp").innerText = "";
      return true;
    }
  };

  this.kiemTraTenHopLe = (list) => {
    let name = document.getElementById("TenSP").value;
    let index = list.findIndex((item) => {
      return item.name == name;
    });
    if (index != -1) {
      document.getElementById("tbTenSp").innerText = "Sản phẩm đã tồn tại";
      return false;
    } else {
      document.getElementById("tbTenSp").innerText = "";
      return true;
    }
  };

  //mô tả không được vượt quá 100 từ
  this.kiemTraMoTaHopLe = () => {
    let description = document.getElementById("MoTa").value;
    let pattern = /[\s\S]{101,}/;
    if (pattern.test(description)) {
      document.getElementById("tbMoTaSp").innerText = "Mô tả vượt quá 100 từ";
      return false;
    } else {
      document.getElementById("tbMoTaSp").innerText = "";
      return true;
    }
  };
}
